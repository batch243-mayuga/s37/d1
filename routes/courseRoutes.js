const express = require("express");
const router = express.Router();
const auth = require("../auth")

const courseControllers = require("../controllers/courseControllers");

// Adding course
router.post("/", auth.verify, courseControllers.addCourse);

// Retrieve all courses
router.get("/allCourses", auth.verify, courseControllers.getAllCourses);

// All active courses
router.get("/allActiveCourses", courseControllers.getAllActive);

// Specific course
router.get("/:courseId", courseControllers.getCourse);

// Update specific course
router.put("/update/:courseId", auth.verify, courseControllers.updateCourse)

// Archive specific course
router.patch("/:courseId/archive", auth.verify, courseControllers.archiveCourse)




module.exports = router;