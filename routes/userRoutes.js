const express = require("express");
const router = express.Router();

const auth = require("../auth")

const userController = require("../controllers/userControllers")

	// Route for checking Email
	router.post("/checkEmail", userController.checkEmailExists);

	// Route for registration
	router.post("/register", userController.checkEmailExists, userController.registerUser);

	// Route for log in
	router.post("/login", userController.loginUser);

	// Route for user details
	router.post("/details", auth.verify,userController.getProfile);

	router.get("/profile", userController.profileDetails);

	// Update role
	router.patch("/updateRole/:userId", auth.verify, userController.updateRole);

	// Enrollment
	router.post("/enroll/:courseId", auth.verify, userController.enroll);


module.exports = router;