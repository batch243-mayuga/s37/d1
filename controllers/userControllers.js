const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");

const auth = require("../auth")

// Check if email already exists
/*
	Steps: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response to the frontend application based on the result of the find method.
*/
module.exports.checkEmailExists = (req, res, next) => {
	// The result is sent back to the frontend via the "then" method
	return User.find({email:req.body.email}).then(result => {
		console.log(req.body.email);
		let message = ``;
			// find method returns an array record of matching documents
		if(result.length > 0){
			message = `The ${req.body.email} is already taken, please use other email.`
			return res.send(message);
		}
			// No duplicate email found
			// The email is not yet registered in the database.
		else{
			next();
		}
	})
}

//registration
module.exports.registerUser = (req, res) => {

	// creates a variable "newUser" and instantiates a new 'User' object using mongoose model
	// Uses the information from request body to provide the necessary information.
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		// salt - salt rounds that bcrypt algorithm will run to encrypt the password
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo
	})

	// Saves the create object to our database.
	return newUser.save().then(user => {
		console.log(user);
		res.send(`Congratulations, Sir/Ma'am ${newUser.firstName}! You are now registered.`)
	}).catch(error => {
		console.log(error);
		res.send(`Sorry ${newUser.firstName}, there was an error during the registration. Please try again!`)
	})
}


// User Authentication
/*
	Steps:
		1. Check database if the user email exist.
		2. Compare the password provided in the login forn with the password stored in the database. 
*/
module.exports.loginUser = (req, res) => {
	// The findOne method, returns the first record in the collection that matches the search criteria. 
	return User.findOne({email:req.body.email}).then(result => {
		console.log(result);
		if(result === null){
			res.send(`Your email:${req.body.email} is not yet registered. Register first!`)
		} else{
				// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
				// The compareSync method is used to compare a non encrypted password from the login form to the encrypted password retrieve. It will return true or false value depending on the result.
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect){

				return res.send({accessToken: auth.createAccessToken(result)});
			} else {
				return res.send(`Incorrect password, please try again!`);
			}
		}
	})
}


// User details
module.exports.getProfile = (req, res) => {
 
	return User.findOne({_id: req.body.id}).then(result => {
		if(result === null){
		return res.send(`Your ID ${req.body.id} not found.`);
	} else {
		result.password = '';
		return res.send(result);
	}
	})	
}


module.exports.profileDetails = (req, res) => {
	// user will be object that contains the Id and email of the user that is currently logged in
	const userData = auth.decode(req.headers.authorization);

	console.log(userData);

	return User.findById(userData.id).then(result => {
		result.password = "Confidential"
		return res.send(result)
	}).catch(err => {
		return res.send(err);
	})
	
}


// Update role

module.exports.updateRole = (req, res) => {
	const token = req.headers.authorization;
	const userData = auth.decode(token);

	let idToBeUpdated = req.params.userId;

	if(userData.isAdmin){

		return User.findById(idToBeUpdated).then(result => {

			let update = {
				isAdmin: !result.isAdmin
			};

			return User.findByIdAndUpdate(idToBeUpdated, update, {new: true}).then(document => {
				document.password = "Confidential";
				return res.send(document)}).catch(err => res.send(err));
			
		}).catch(err => res.send(err));

	} else{
		return res.send("You don't have access on this page!")
	}
}



// Enrollment

module.exports.enroll = async (req, res) => {
	const token = req.headers.authorization;
	let userData = auth.decode(token);

	if(!userData.isAdmin){
		let courseId = req.params.courseId;

		let data = {
			courseId: courseId,
			userId: userData.id
		}

			let isCourseUpdated = await Course.findById(courseId).then(result => {
				result.enrollees.push({
					userId: data.userId
				})

				result.slots -= 1;

				return result.save().then(success => {
					return true
				}).catch(err => {
					return false
			});
			}).catch(err => {
				console.log(err);
				return res.send(false);
			})

			let isUserUpdated = await User.findById(data.userId).then(result => {
				result.enrollments.push({
					courseId: data.courseId
				})

				return result.save().then(success => {
					return true;
				}).catch(err => {return false;})

			}).catch(err => {return res.send(false)});

			(isUserUpdated && isCourseUpdated) ? res.send("You are now enrolled!") : res.send("We encountered an error in your enrollment, please try again!")

	}
	else{
		return res.send("You are admin, you cannot enroll to course.")
	}
}