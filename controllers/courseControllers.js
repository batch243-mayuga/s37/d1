const Course = require("../models/Course");
const auth = require("../auth");

/*
	Steps:
	1. Create a new Course object using mongoose model and the information from the request of the user.
	2. Save the new Course to the database
*/
module.exports.addCourse = (req, res) => {

	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		slots: req.body.slots
	})
	
	const token = req.headers.authorization;
	const userData = auth.decode(token);
	console.log(userData);

	if(userData.isAdmin){

	newCourse.save().then(result => {
		console.log(result)
		res.send(true)
	}).catch(error => {
		console.log(error);
		res.send(false);
	})
}else {
	res.send("You are not an admin!");
}

}


// Retrieve all active courses

module.exports.getAllActive = (req, res) => {
	return Course.find({isActive: true}).then(result => {
		res.send(result);
	}).catch(err => {
		res.send(err);
	})
}


// Retrieving a specific course

module.exports.getCourse = (req, res) => {
	const courseId = req.params.courseId;

	return Course.findById(courseId).then(result => {
		res.send(result);
	}).catch(err => {
		res.send(err);
	})
}


// Update a specific course

module.exports.updateCourse = (req, res) => {
	const token = req.headers.authorization;
	const userData = auth.decode(token);

	console.log(userData);

		let updatedCourse = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		}

		const courseId = req.params.courseId;

	if(userData.isAdmin){
		return Course.findByIdAndUpdate(courseId, updatedCourse, {new:true}).then(result =>{
			res.send(result)
		}).catch(err => {
			res.send(err);
		})
	} else {
		return res.send(`You dont have access to this page!`);
	}
}


// Archiving a specific course

module.exports.archiveCourse = (req, res) => {
	const token = req.headers.authorization;
	const userData = auth.decode(token);

	console.log(userData);

		let archivedCourse = {
			isActive: req.body.isActive
		}

		const courseId = req.params.courseId;

	if(userData.isAdmin){
		return Course.findByIdAndUpdate(courseId, archivedCourse, {new:true}).then(result =>{

			return res.send(true);

		}).catch(err => {
			console.log(err);
			res.send(false);
		})
	} else {
		return res.send(`You dont have access to this page!`);
	}
}


// Retrieve all course including those inactive course
/*
	Steps:
		1. Retrieve all the course (active/inactive) from the database.
		2. verify the role of the current user(admin to continue)
*/

module.exports.getAllCourses = (req, res) => {
	const token = req.headers.authorization;
	const userData = auth.decode(token);

	if(!userData.isAdmin){
		return res.send("Sorry, you don't have access to this page!")
	} else{
		return Course.find({}).then(result => res.send(result)).catch(err => {
			console.log(err);
			res.send(err);
		})
	}
}


