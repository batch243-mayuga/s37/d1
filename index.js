// Setup dependencies

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// Allows access to routes defined within our application
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");
const app = express();

// Middlewares
// Allows all resources to access our backend application
app.use(cors());
app.use(express());

// express.json() function is a built-in middleware function in express. It parses incoming request with JSON payloads.
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" to be included for all users routes defined in the userRoutes route file.
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// Database connection

mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.cyvdebm.mongodb.net/bookingAPI?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	})


mongoose.connection.on("error", console.error.bind(console, "connection error"));
mongoose.connection.once('open',() => console.log("Now connected to MongoDB Atlas"));


// This syntax will allow flexibility when using the application both locally and as hosted app
// process.env.PORT is that can be assigned by your hosting service
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT||4000}`);
})